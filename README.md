# Quake Log Parser #

### What is this repository for? ###
- Repositório criado para realizar um parser no arquivo de log do jogo quake;
- Foi utilizado a linguagem de programação de altíssimo nível Python 2.7;
- Foi utilizado a lib "logging" para imprimir as informações necessárias do parser;
- Foi utilizado a lib "re" para ler a linha de acordo com a keyword(InitGame, ShutdownGame, ClientUserinfoChanged e Kill);
- Foi utilizado o arquivo requirements.txt para armazenar as dependências do projeto;
- Foi utilizado o "doctest" nativo do Python para realizar os testes do parser;
- Não foi utilziado nenhum Framework para o desafio;
- Foi criado o arquivo "gitignore" de acordo com as recomendações da comunidade git para o Python;
- Foi criado comentários na doc de cada função.



* Version 1.0

### Is necessary installed pip-python.

### How do I get set up? ###

* Summary of set up
- $ pip install -r requirements.txt

* Dependencies [requirements.txt file]
- Python 2.7
- Eve==0.6.3

* How to run tests [doctests]
- $ python main.py -v
- $ python parser.py -v


* How to run the projet[port=700, host=localhost]
- $ python main.py 

- See here: http://localhost:7000/v1/game/log

