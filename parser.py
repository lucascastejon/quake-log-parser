#!/Users/lucascastejon/projetos/quake-log-parser/bin/python
# -*- coding: UTF-8 -*-

import logging
import re

logging.basicConfig(level=logging.INFO,
                    format='[%(levelname)s][%(asctime)s][%(module)s][%(lineno)s] %(message)s',
                    datefmt='%H:%M:%S')

logger = logging.getLogger(__name__)

# Analisando game.log 
# > InitGame              - inicia o jogo
# > ShutdownGame          - fim de jogo
# > ClientConnect         - Players no jogo
# > ClientUserinfoChanged - informações do player

# Variables global
game_quake = {}
game_count = 0
kills      = []

# 
# cleanLastLines recebe a linha dos kills no formado de lista
# o "del" realiza a exclusão do index apontado pela lista
# 
def cleanLastLines(kill_s):
	"""
		Remove index of dict to skimp process
	"""
	del kill_s[0]
	del kill_s[0]
	del kill_s[0]
	del kill_s[0]
	del kill_s[0]
	del kill_s[-1]
	del kill_s[-1]

	return kill_s

# 
# resolveKills em um único loop cria o dict contendo o nome e a quantidade de kills
# 
def resolveKills(data_line_kills):
	"""
		Make total of kill for each player
	"""
	kills = {}

	logger.info("Game: %d ", game_count)
	for kill in data_line_kills:
		kill_s = cleanLastLines(kill.split())
		killed = kill_s[:kill_s.index("killed"):-1]
		killed.reverse()
		player_name = str(' '.join(killed))

		try:
			if kill_s[0] == '<world>':
				if kills[player_name] > 0:
					kills[player_name] -= 1
			else:
				kills[player_name] += 1
		except:
			kills[player_name] = 0
	
	print("kills2")
	print(kills)
	return kills

# 
# parseCreate abre o arquivo games.log e realiza a leitura linha a linha, com a ajuda da lib "re"
# foi possível ler a keyword para assim realizar o parser 
def parseCreate():
	"""
	>>> parseCreate()
	{0: {'players': ['Isgalamido'], 'kills': {}, 'total_kills': 0}, 1: {'players': ['Dono', 'Mocinha', 'Isgalamido', 'Zeh'], 'kills': {'Mocinha': 0, 'Zeh': 0, 'Dono da Bola': 0}, 'total_kills': 4}, 2: {'players': ['Dono', 'Isgalamido', 'Zeh', 'Assasinu'], 'kills': {'Zeh': 22, 'Isgalamido': 10, 'Assasinu Credi': 18, 'Dono da Bola': 18}, 'total_kills': 105}, 3: {'players': ['Dono', 'Isgalamido', 'Zeh', 'Assasinu'], 'kills': {'Zeh': 2, 'Assasinu Credi': 1, 'Dono da Bola': 0}, 'total_kills': 14}, 4: {'players': ['Fasano', 'Oootsimo', 'Isgalamido', 'Zeh', 'Dono', 'UnnamedPlayer', 'Maluquinho', 'Assasinu', 'Mal'], 'kills': {'Zeh': 5, 'Maluquinho': 0, 'Mal': 1, 'Assasinu Credi': 2, 'Dono da Bola': 4, 'Oootsimo': 1, 'Isgalamido': 4, 'UnnamedPlayer': 0}, 'total_kills': 29}, 5: {'players': ['Oootsimo', 'Isgalamido', 'Zeh', 'Dono', 'Mal', 'Assasinu', 'Chessus!', 'Chessus'], 'kills': {'Chessus': 1, 'Isgalamido': 6, 'Mal': 4, 'Assasinu Credi': 18, 'Dono da Bola': 23, 'Oootsimo': 10, 'Zeh': 13}, 'total_kills': 130}, 6: {'players': ['Oootsimo', 'Isgalamido', 'Zeh', 'Dono', 'Mal', 'Assasinu'], 'kills': {'Isgalamido': 4, 'Mal': 13, 'Assasinu Credi': 12, 'Dono da Bola': 11, 'Oootsimo': 13, 'Zeh': 5}, 'total_kills': 89}, 7: {'players': ['Oootsimo', 'Isgalamido', 'Zeh', 'Dono', 'Mal', 'Assasinu', 'Chessus!', 'Chessus'], 'kills': {'Chessus': 1, 'Isgalamido': 1, 'Mal': 10, 'Assasinu Credi': 13, 'Dono da Bola': 2, 'Oootsimo': 11, 'Zeh': 8}, 'total_kills': 67}, 8: {'players': ['Oootsimo', 'Dono', 'Zeh', 'Chessus', 'Mal', 'Assasinu', 'Isgalamido'], 'kills': {'Isgalamido': 4, 'Oootsimo': 4, 'Zeh': 2, 'Assasinu Credi': 4, 'Dono da Bola': 0, 'Chessus': 6, 'Mal': 4}, 'total_kills': 60}, 9: {'players': ['Dono', 'Isgalamido', 'Zeh', 'Oootsimo', 'Chessus', 'Assasinu', 'UnnamedPlayer', 'Mal'], 'kills': {'Oootsimo': 0, 'Zeh': 1, 'Mal': 0, 'Assasinu Credi': 0, 'Dono da Bola': 2, 'Chessus': 2, 'Isgalamido': 2}, 'total_kills': 20}, 10: {'players': ['Isgalamido', 'Dono', 'Zeh', 'Oootsimo', 'Chessus', 'Assasinu', 'Mal'], 'kills': {'Chessus': 16, 'Isgalamido': 20, 'Mal': 0, 'Assasinu Credi': 12, 'Dono da Bola': 16, 'Oootsimo': 4, 'Zeh': 13}, 'total_kills': 160}, 11: {'players': ['Isgalamido', 'Dono', 'Zeh', 'Oootsimo', 'Chessus', 'Assasinu', 'Mal'], 'kills': {'Isgalamido': 0, 'Oootsimo': 0, 'Assasinu Credi': 1, 'Dono da Bola': 1}, 'total_kills': 6}, 12: {'players': ['Isgalamido', 'Dono', 'Zeh', 'Oootsimo', 'Chessus', 'Assasinu', 'Mal'], 'kills': {'Chessus': 7, 'Isgalamido': 6, 'Mal': 5, 'Assasinu Credi': 9, 'Dono da Bola': 10, 'Oootsimo': 5, 'Zeh': 8}, 'total_kills': 122}, 13: {'players': ['Zeh', 'Assasinu', 'Dono', 'Fasano', 'Isgalamido', 'Oootsimo'], 'kills': {'Zeh': 0}, 'total_kills': 3}, 14: {'players': ['Dono', 'Oootsimo', 'Isgalamido', 'Assasinu', 'Zeh'], 'kills': {}, 'total_kills': 0}, 15: {'players': ['Dono', 'Oootsimo', 'Isgalamido', 'Assasinu', 'Zeh', 'UnnamedPlayer', 'Mal'], 'kills': {'Isgalamido': 0, 'Mal': 0, 'Assasinu Credi': 0, 'Dono da Bola': 0, 'Oootsimo': 2, 'Zeh': 1}, 'total_kills': 13}, 16: {'players': ['Dono', 'Oootsimo', 'Isgalamido', 'Assasinu', 'Zeh', 'Mal'], 'kills': {'Zeh': 0, 'Mal': 0, 'Assasinu Credi': 0, 'Dono da Bola': 0, 'Oootsimo': 0, 'Isgalamido': 0}, 'total_kills': 7}, 17: {'players': ['Isgalamido', 'Oootsimo', 'Dono', 'Assasinu', 'Zeh', 'Mal'], 'kills': {'Isgalamido': 10, 'Mal': 8, 'Assasinu Credi': 11, 'Dono da Bola': 13, 'Oootsimo': 11, 'Zeh': 15}, 'total_kills': 95}, 18: {'players': ['Isgalamido', 'Oootsimo', 'Dono', 'Assasinu', 'Zeh', 'Mal'], 'kills': {'Zeh': 0, 'Assasinu Credi': 0, 'Dono da Bola': 0}, 'total_kills': 3}, 19: {'players': ['Isgalamido', 'Oootsimo', 'Dono', 'Assasinu', 'Zeh', 'Mal'], 'kills': {'Zeh': 10, 'Mal': 19, 'Assasinu Credi': 23, 'Dono da Bola': 16, 'Oootsimo': 13, 'Isgalamido': 15}, 'total_kills': 131}}
	
	"""
	game_quake = {}
	game_count = 0
	kills      = []

	with open('games.log', 'r') as log_file:
		for log_line in log_file:
			if re.search(r'InitGame', log_line):
				kills      = []
				game_quake[game_count] = {}
				game_quake[game_count]['total_kills'] = 0
				game_quake[game_count]['players']     = []
				game_quake[game_count]['kills']       = []

			elif re.search( r'ShutdownGame', log_line):
				# Quantidade de jogos
				game_quake[game_count]['kills'] = resolveKills(kills)
				game_count += 1

			elif re.search( r'ClientUserinfoChanged', log_line):
				player_name = log_line.split()[3].split("\\")[1]

				# Faz o append apenas se não existir o player
				try:
					game_quake[game_count]['players'].index(player_name)
				except:
					game_quake[game_count]['players'].append(player_name)

			elif re.search( r'Kill', log_line):
				kills.append(log_line) # adiciona as linhas de kills para a função resolveKills
				game_quake[game_count]['total_kills'] += 1

			else:
				pass
	log_file.closed

	return game_quake

# doctest executa os teste que está na descrição
# para executar, basta chamar o arquivo assim: $ python parser.py -v
import doctest
doctest.testmod()

