#!/Users/lucascastejon/projetos/quake-log-parser/bin/python
# -*- coding: UTF-8 -*-

from parser import parseCreate as p
from eve import Eve
import json

port, host = 7000, '127.0.0.1'

app = Eve()

@app.route('/v1/game/log')
def apiGameLog():
	"""
	>>> apiGameLog()
	200
	"""
	return json.dumps(str(p()))

if __name__ == '__main__':
    app.run(host=host, port=port)

import doctest
doctest.testmod()